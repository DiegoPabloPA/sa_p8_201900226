const express = require('express');
const conexion = require('./conexion');
const app = express();



const port = 8000; 

app.get('/', async (req, res) => {
  conexion().then(async db => {
      const collection = await db.collection('alumnos');

      let result=await collection.find().toArray()
      res.send(result)
    })
    
  });
  



app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});