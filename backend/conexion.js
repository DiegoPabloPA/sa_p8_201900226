const { MongoClient } = require('mongodb');

const uri = 'mongodb://database:27017';
const dbName = 'practica7';

async function conexion() {
  try {
    const client = await MongoClient.connect(uri);
    const db = client.db(dbName);
    return db;
  } catch (err) {
    console.error('Error al conectar a la base de datos:', err);
    throw err;
  }
}

module.exports = conexion;